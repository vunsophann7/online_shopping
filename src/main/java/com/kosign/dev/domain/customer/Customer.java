package com.kosign.dev.domain.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kosign.dev.domain.order.Order;
import com.kosign.dev.domain.payment.Payment;
import com.kosign.dev.domain.shipment.Shipment;
import com.kosign.dev.domain.wishlist.Wishlist;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "customers")
@Where(clause = "status = '1'")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private String address;
    private String phone_number;
    private String status;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<Shipment> shipments;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<Order> orders;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<Payment> payments;


    @OneToMany(mappedBy = "customer")
    private List<Wishlist> wishlists;



    @Builder
    public Customer(String first_name, String last_name, String email, String password, String address, String phone_number, String status) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phone_number = phone_number;
        this.status = status;
    }
}
