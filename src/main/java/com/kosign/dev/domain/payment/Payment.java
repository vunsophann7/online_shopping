package com.kosign.dev.domain.payment;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.order.Order;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "payments")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime payment_date;
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "payment")
    private List<Order> orders;

    @Builder
    public Payment(LocalDateTime payment_date, BigDecimal amount, Customer customer) {
        this.payment_date = payment_date;
        this.amount = amount;
        this.customer = customer;
    }
}
