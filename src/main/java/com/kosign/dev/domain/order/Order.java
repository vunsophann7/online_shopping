package com.kosign.dev.domain.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.payment.Payment;
import com.kosign.dev.domain.shipment.Shipment;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders")
@Where(clause = "status = '1'")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalTime order_date;
    private BigDecimal total_price;
    private String status;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "payment_id")
    @JsonIgnore
    private Payment payment;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    @JsonIgnore
    private Shipment shipments;

    @Builder
    public Order(LocalTime order_date, BigDecimal total_price, Customer customer, Payment payment, Shipment shipments, String status) {
        this.order_date = order_date;
        this.total_price = total_price;
        this.customer = customer;
        this.payment = payment;
        this.shipments = shipments;
        this.status = status;
    }
}
