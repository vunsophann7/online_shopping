package com.kosign.dev.domain.order;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE Order SET status = '9' WHERE id = ?1")
    void deleteOrderById(Long id);
}
