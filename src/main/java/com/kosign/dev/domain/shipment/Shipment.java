package com.kosign.dev.domain.shipment;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.order.Order;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "shipments")
public class Shipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime shipment_date;
    private String address;
    private String city;
    private String state;
    private String country;
    private String zip_code;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "shipments")
    private List<Order> orders;

    @Builder
    public Shipment(LocalDateTime shipment_date, String address, String city, String state, String country, String zip_code, Customer customer) {
        this.shipment_date = shipment_date;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip_code = zip_code;
        this.customer = customer;
    }
}
