package com.kosign.dev.controller.customer;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.payload.customer.CustomerRequest;
import com.kosign.dev.service.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/customers")
public class CustomerController extends AbstractRestController {
    private final CustomerService customerService;

    @PostMapping
    public Object createCustomer(@RequestBody CustomerRequest customerRequest) {
        customerService.createCustomer(customerRequest);
        return ok();
    }

    @GetMapping("/{id}")
    public Object getCustomerById(@PathVariable(name = "id") Long id) {
        Customer customer = customerService.getCustomerById(id);
        return ok(customer);
    }

    @GetMapping
    public Object getCustomerList() {
        return ok(customerService.getCustomerList());
    }

    @PutMapping("/{id}")
    public Object updateCustomer(
            @PathVariable(name = "id") Long id,
            @RequestBody CustomerRequest customerRequest
    ) {
        return customerService.updateCustomer(id, customerRequest);
    }

    @DeleteMapping("/{id}")
    public Object deleteCustomer(@PathVariable(name = "id") Long id) {
        customerService.deleteCustomer(id);
        return ok();
    }
}
