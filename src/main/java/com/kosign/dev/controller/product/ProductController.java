package com.kosign.dev.controller.product;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.payload.product.ProductMainResponse;
import com.kosign.dev.payload.product.ProductRequest;
import com.kosign.dev.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
public class ProductController extends AbstractRestController {
    private final ProductService productService;

    @PostMapping
    public Object createProduct(@RequestBody ProductRequest productRequest) {
        productService.createProduct(productRequest);
        return ok();
    }

    @GetMapping
    public Object getProductList() {
        return ok(productService.getProductList());
    }

    @GetMapping("/{id}")
    public Object getProductById(@PathVariable(value = "id") Long id) {
        var productId = productService.getProductById(id);
        return ok(productId);
    }

    @PutMapping("/{id}")
    public Object updateProduct(
            @PathVariable(value = "id") Long id,
            @RequestBody ProductRequest productRequest
            ) {
        productService.updateProduct(id, productRequest);
        return ok();
    }

    @DeleteMapping("/{id}")
    public Object deleteProduct(@PathVariable(value = "id") Long id) {
        productService.deleteProduct(id);
        return ok();
    }
}
