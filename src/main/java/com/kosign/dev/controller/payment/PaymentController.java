package com.kosign.dev.controller.payment;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.payload.payment.PaymentRequest;
import com.kosign.dev.service.payment.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/payments")
@RequiredArgsConstructor
public class PaymentController extends AbstractRestController {
    private final PaymentService paymentService;

    @PostMapping
    public Object createPayment(@RequestBody PaymentRequest paymentRequest) {
        paymentService.createPayment(paymentRequest);
        return ok();
    }

    @GetMapping
    public Object getPaymentList() {
        return ok(paymentService.getPaymentList());
    }

    @GetMapping("/{id}")
    public Object getPaymentById(@PathVariable(value = "id") Long id) {
        return ok(paymentService.getPaymentById(id));
    }
}
