package com.kosign.dev.controller.shipment;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.payload.shipment.ShipmentRequest;
import com.kosign.dev.service.shipment.ShipmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/shipments")
@RequiredArgsConstructor
public class ShipmentController extends AbstractRestController {

    private final ShipmentService shipmentService;

    @PostMapping
    public Object createShipment(@RequestBody ShipmentRequest shipmentRequest) {
        shipmentService.createShipment(shipmentRequest);
        return ok();
    }

    @GetMapping("/{id}")
    public Object getShipmentById(@PathVariable(value = "id") Long id) {
        return ok(shipmentService.getShipmentById(id));
    }

    @GetMapping
    public Object getShipmentList() {
        return ok(shipmentService.getShipmentList());
    }

    @PutMapping("/{id}")
    public Object updateShipment(@PathVariable(value = "id") Long id, @RequestBody ShipmentRequest shipmentRequest) {
        shipmentService.updateShipment(id, shipmentRequest);
        return ok();
    }
}
