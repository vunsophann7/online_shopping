package com.kosign.dev.controller.order;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.domain.order.Order;
import com.kosign.dev.payload.order.OrderRequest;
import com.kosign.dev.payload.order.OrderResponse;
import com.kosign.dev.service.order.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/orders")
public class OrderController extends AbstractRestController {
    private final OrderService orderService;

    @PostMapping
    public Object createOrder(@RequestBody OrderRequest orderRequest) {
        orderService.createOrder(orderRequest);
        return ok();
    }

    @GetMapping
    public Object getOrderList() {
        return ok(orderService.getOrderList());
    }

    @GetMapping("/{id}")
    public Object getOrderById(@PathVariable(value = "id") Long id) {
        return ok(orderService.getOrderById(id));
    }

    @PutMapping("/{id}")
    public Object updateOrder(
            @PathVariable(value = "id") Long id,
            @RequestBody OrderRequest orderRequest
    ) {
        orderService.updateOrder(id, orderRequest);
        return ok();
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable(value = "id") Long id) {
        orderService.deleteOrder(id);
    }


}


