package com.kosign.dev.payload.wishList;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.product.Product;
import com.kosign.dev.payload.customer.CustomerResponse;
import com.kosign.dev.payload.product.ProductResponse;
import lombok.Builder;

public class WishListResponse {
    private Long id;
    private Long customer_id;
    private Long product_id;

    @Builder
    public WishListResponse(Long customer_id, Long product_id) {
        this.customer_id = customer_id;
        this.product_id = product_id;
    }
}
