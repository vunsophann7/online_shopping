package com.kosign.dev.payload.wishList;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.product.Product;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WishListRequest {
    private Long customer_id;
    private Long product_id;

    @Builder
    public WishListRequest(Long customer_id, Long product_id) {
        this.customer_id = customer_id;
        this.product_id = product_id;
    }
}
