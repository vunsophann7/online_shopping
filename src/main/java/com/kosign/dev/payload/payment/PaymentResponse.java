package com.kosign.dev.payload.payment;

import com.kosign.dev.domain.customer.Customer;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PaymentResponse {

    private Long id;
    private LocalDateTime payment_date;
    private BigDecimal amount;
    private Customer customer;

    @Builder
    public PaymentResponse(LocalDateTime payment_date, BigDecimal amount, Customer customer) {
        this.payment_date = payment_date;
        this.amount = amount;
        this.customer = customer;
    }
}
