package com.kosign.dev.payload.payment;

import com.kosign.dev.domain.customer.Customer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class PaymentRequest {
    private LocalTime payment_date;
    private BigDecimal amount;
    private Long customer;


}
