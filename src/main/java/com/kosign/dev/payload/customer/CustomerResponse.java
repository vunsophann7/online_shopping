package com.kosign.dev.payload.customer;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomerResponse {
    private Long customer_id;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private String address;
    private String phone_number;


    @Builder
    public CustomerResponse(Long customer_id, String first_name, String last_name, String email, String password, String address, String phone_number) {
        this.customer_id = customer_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phone_number = phone_number;
    }
}
