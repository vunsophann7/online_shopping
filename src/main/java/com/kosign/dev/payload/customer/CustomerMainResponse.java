package com.kosign.dev.payload.customer;

import java.util.List;

public record CustomerMainResponse(
         List<CustomerResponse> customer

) {
}
