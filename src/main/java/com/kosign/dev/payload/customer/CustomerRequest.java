package com.kosign.dev.payload.customer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomerRequest {
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private String address;
    private String phone_number;
}
