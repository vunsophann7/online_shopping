package com.kosign.dev.payload.order;

import java.util.List;

public record OrderMainResponse(
        List<OrderResponse> orderData
) {
}
