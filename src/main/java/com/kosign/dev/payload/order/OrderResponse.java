package com.kosign.dev.payload.order;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.payment.Payment;
import com.kosign.dev.domain.shipment.Shipment;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
public class OrderResponse {
    private LocalTime order_date;
    private BigDecimal total_price;
    private Customer customer;
    private Payment payment;
    private Shipment shipments;


    @Builder
    public OrderResponse(LocalTime order_date, BigDecimal total_price, Customer customer, Payment payment, Shipment shipments) {
        this.order_date = order_date;
        this.total_price = total_price;
        this.customer = customer;
        this.payment = payment;
        this.shipments = shipments;
    }
}
