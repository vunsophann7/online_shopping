package com.kosign.dev.payload.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
public class OrderRequest {
    private Timestamp datetime;
    private BigDecimal price;
    private Long customer_id;
    private Long payment_id;
    private Long shipment_id;
}
