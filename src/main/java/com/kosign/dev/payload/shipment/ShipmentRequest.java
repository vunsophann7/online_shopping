package com.kosign.dev.payload.shipment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ShipmentRequest {

    private LocalDateTime shipment_date;
    private String address;
    private String city;
    private String state;
    private String country;
    private String zip_code;
    private Long customer_id;


}
