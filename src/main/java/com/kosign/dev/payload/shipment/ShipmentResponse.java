package com.kosign.dev.payload.shipment;

import com.kosign.dev.domain.customer.Customer;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ShipmentResponse {
    private Long id;
    private LocalDateTime shipment_date;
    private String address;
    private String city;
    private String state;
    private String country;
    private String zip_code;
    private Customer customer;

    @Builder
    public ShipmentResponse(Long id, LocalDateTime shipment_date, String address, String city, String state, String country, String zip_code, Customer customer) {
        this.id = id;
        this.shipment_date = shipment_date;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip_code = zip_code;
        this.customer = customer;
    }
}
