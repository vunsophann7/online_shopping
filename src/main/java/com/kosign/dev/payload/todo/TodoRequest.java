package com.kosign.dev.payload.todo;

public record TodoRequest(

        String title,

        String desc
) {
}
