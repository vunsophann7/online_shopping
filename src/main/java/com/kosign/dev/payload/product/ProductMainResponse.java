package com.kosign.dev.payload.product;

import java.util.List;

public record ProductMainResponse(
        List<ProductResponse> productData
) {
}
