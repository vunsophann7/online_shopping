package com.kosign.dev.service.payment;

import com.kosign.dev.domain.payment.Payment;
import com.kosign.dev.payload.payment.PaymentRequest;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {

    void createPayment(PaymentRequest paymentRequest);
    Payment getPaymentById(Long id);
    Object getPaymentList();


}
