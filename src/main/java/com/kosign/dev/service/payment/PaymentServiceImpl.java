package com.kosign.dev.service.payment;

import com.kosign.dev.domain.payment.Payment;
import com.kosign.dev.domain.payment.PaymentRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.payment.PaymentRequest;
import com.kosign.dev.payload.payment.PaymentResponse;
import com.kosign.dev.service.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final CustomerService customerService;
    @Override
    public void createPayment(PaymentRequest paymentRequest) {
        var paymentEntity = Payment.builder()
                .payment_date(LocalDateTime.now())
                .amount(paymentRequest.getAmount())
                .customer(customerService.getCustomerById(paymentRequest.getCustomer()))
                .build();
        paymentRepository.save(paymentEntity);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentRepository.findById(id).orElseThrow(() -> new CusNotFoundException("Payment not found!!!"));
    }

    @Override
    public Object getPaymentList() {
        var paymentEntity = paymentRepository.findAll();
        List<PaymentResponse> paymentResponses = paymentEntity.stream()
                .map(payment -> PaymentResponse.builder()
                        .payment_date(payment.getPayment_date())
                        .amount(payment.getAmount())
                        .customer(payment.getCustomer())
                        .build()
                ).collect(Collectors.toList());
        return paymentResponses;
    }
}
