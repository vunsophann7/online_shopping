package com.kosign.dev.service.customer;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.payload.customer.CustomerRequest;

public interface CustomerService {
    void createCustomer(CustomerRequest customerRequest);
    Customer getCustomerById(Long id);
    Object getCustomerList();
    Object updateCustomer(Long id, CustomerRequest customerRequest);
    void deleteCustomer(Long id);
}
