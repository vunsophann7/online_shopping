package com.kosign.dev.service.customer;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.customer.CustomerRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.customer.CustomerMainResponse;
import com.kosign.dev.payload.customer.CustomerRequest;
import com.kosign.dev.payload.customer.CustomerResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    @Override
    public void createCustomer(CustomerRequest customerRequest) {
        var customerEntity = Customer.builder()
                .first_name(customerRequest.getFirst_name())
                .last_name(customerRequest.getLast_name())
                .email(customerRequest.getEmail())
                .password(customerRequest.getPassword())
                .address(customerRequest.getAddress())
                .phone_number(customerRequest.getPhone_number())
                .status("1")
                .build();
        customerRepository.save(customerEntity);
    }

    @Override
    public Customer getCustomerById(Long id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new CusNotFoundException("Customer not found!!!"));
    }

    @Override
    public Object getCustomerList() {
        var customerList = customerRepository.findAll();
        List<CustomerResponse> customerResponses = customerList.stream()
                .map(customer -> CustomerResponse.builder()
                        .customer_id(customer.getId())
                        .first_name(customer.getFirst_name())
                        .last_name(customer.getLast_name())
                        .email(customer.getEmail())
                        .password(customer.getPassword())
                        .address(customer.getAddress())
                        .phone_number(customer.getPhone_number())
                        .build()
                ).collect(Collectors.toList());
        return new CustomerMainResponse(customerResponses);
    }

    @Override
    public Object updateCustomer(Long id, CustomerRequest customerRequest) {
        var customerEntity = getCustomerById(id);
        customerEntity.setFirst_name(customerRequest.getFirst_name());
        customerEntity.setLast_name(customerRequest.getLast_name());
        customerEntity.setEmail(customerRequest.getEmail());
        customerEntity.setPassword(customerRequest.getPassword());
        customerEntity.setAddress(customerRequest.getAddress());
        customerEntity.setPhone_number(customerRequest.getPhone_number());
        return customerRepository.save(customerEntity);
    }

    @Override
    public void deleteCustomer(Long id) {
        customerRepository.deleteCustomerByCustomer_id(id);
    }
}
