package com.kosign.dev.service.wishlist;

import com.kosign.dev.payload.wishList.WishListRequest;

public interface WishListService {
    void createWishList(WishListRequest wishListRequest);
    Object getWishListById(Long id);
    Object getWishListByList();
    Object updateWishList(Long id, WishListRequest wishListRequest);
    void deleteWishList(Long id);

}
