package com.kosign.dev.service.wishlist;

import com.kosign.dev.domain.customer.Customer;
import com.kosign.dev.domain.customer.CustomerRepository;
import com.kosign.dev.domain.product.Product;
import com.kosign.dev.domain.product.ProductRepository;
import com.kosign.dev.domain.wishlist.Wishlist;
import com.kosign.dev.domain.wishlist.WishlistRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.wishList.WishListRequest;
import com.kosign.dev.payload.wishList.WishListResponse;
import com.kosign.dev.service.customer.CustomerService;
import com.kosign.dev.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WishListServiceImpl implements WishListService {
    private final WishlistRepository wishlistRepository;
    private final CustomerService customerService;
    private final ProductService productService;
    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;
    @Override
    public void createWishList(WishListRequest wishListRequest) {
        Product productId = productRepository.findById(wishListRequest.getProduct_id())
                .orElseThrow(() -> new CusNotFoundException("Product not found!!!"));
        var wishListEntity = Wishlist.builder()
                .customer(customerService.getCustomerById(wishListRequest.getCustomer_id()))
                .product(productId)
                        .build();
        wishlistRepository.save(wishListEntity);
    }

    @Override
    public Object getWishListById(Long id) {
//        Customer customerId = customerRepository.findById(id).orElseThrow(() -> new CusNotFoundException("Customer not found!!!"));
//        WishListResponse wishListId = wishlistRepository.findById(id)
//                .map(wishlist -> WishListResponse.builder()
//                        .customer_id(customerId)
        return null;
    }

    @Override
    public Object getWishListByList() {
        return null;
    }

    @Override
    public Object updateWishList(Long id, WishListRequest wishListRequest) {
        return null;
    }

    @Override
    public void deleteWishList(Long id) {

    }
}
