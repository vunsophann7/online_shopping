package com.kosign.dev.service.order;

import com.kosign.dev.payload.order.OrderRequest;

public interface OrderService {
    void createOrder(OrderRequest orderRequest);
    Object getOrderList();
    Object getOrderById(Long id);
    Object updateOrder(Long id, OrderRequest orderRequest);
    void deleteOrder(Long id);
}
