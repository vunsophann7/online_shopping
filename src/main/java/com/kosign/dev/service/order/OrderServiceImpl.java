package com.kosign.dev.service.order;

import com.kosign.dev.domain.order.Order;
import com.kosign.dev.domain.order.OrderRepository;
import com.kosign.dev.domain.shipment.Shipment;
import com.kosign.dev.domain.shipment.ShipmentRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.order.OrderMainResponse;
import com.kosign.dev.payload.order.OrderRequest;
import com.kosign.dev.payload.order.OrderResponse;
import com.kosign.dev.service.customer.CustomerService;
import com.kosign.dev.service.payment.PaymentService;
import com.kosign.dev.service.shipment.ShipmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerService customerService;
    private final PaymentService paymentService;
    private final ShipmentService shipmentService;
    private final ShipmentRepository shipmentRepository;

    @Override
    public void createOrder(OrderRequest orderRequest) {
        Shipment shipmentId = shipmentRepository.findById(orderRequest.getShipment_id())
                .orElseThrow(() -> new CusNotFoundException("Shipment not found!!!"));
        var orderEntity = Order.builder()
                .order_date(LocalTime.now())
                .total_price(orderRequest.getPrice())
                .customer(customerService.getCustomerById(orderRequest.getCustomer_id()))
                .payment(paymentService.getPaymentById(orderRequest.getPayment_id()))
                .shipments(shipmentId)
                .status("1")
                .build();
        orderRepository.save(orderEntity);
    }

    @Override
    public Object getOrderList() {
        var orderList = orderRepository.findAll();
        List<OrderResponse> orderResponses = orderList.stream()
                .map(order -> OrderResponse.builder()
                        .order_date(order.getOrder_date())
                        .total_price(order.getTotal_price())
                        .customer(order.getCustomer())
                        .payment(order.getPayment())
                        .shipments(order.getShipments())
                        .build()
                ).collect(Collectors.toList());
        return new OrderMainResponse(orderResponses);
    }

    @Override
    public Object getOrderById(Long id) {
        var orderResponse = orderRepository.findById(id)
                .map(order -> OrderResponse.builder()
                        .order_date(order.getOrder_date())
                        .total_price(order.getTotal_price())
                        .customer(order.getCustomer())
                        .payment(order.getPayment())
                        .shipments(order.getShipments())
                        .build()
                )
                .orElseThrow(() -> new CusNotFoundException("Order not found!!!"));
        return orderResponse;
    }

    @Override
    public Object updateOrder(Long id, OrderRequest orderRequest) {
//        var orderFound = getOrderById(id);
        var orderFound = orderRepository.findById(id)
                        .orElseThrow(() -> new CusNotFoundException("Order not found!!!"));
        Shipment shipmentId = shipmentRepository.findById(orderRequest.getShipment_id())
                .orElseThrow(() -> new CusNotFoundException("Shipment not found!!!"));
        orderFound.setTotal_price(orderRequest.getPrice());
        orderFound.setPayment(paymentService.getPaymentById(orderRequest.getPayment_id()));
        orderFound.setCustomer(customerService.getCustomerById(orderRequest.getCustomer_id()));
        orderFound.setShipments(shipmentId);
        return orderRepository.save(orderFound);
    }

    @Override
    public void deleteOrder(Long id) {
        orderRepository.deleteOrderById(id);
    }
}
