package com.kosign.dev.service.product;

import com.kosign.dev.domain.product.Product;
import com.kosign.dev.payload.product.ProductMainResponse;
import com.kosign.dev.payload.product.ProductRequest;

public interface ProductService {
    void createProduct(ProductRequest productRequest);
    Object getProductById(Long id);
    Object getProductList();
    Object updateProduct(Long id, ProductRequest productRequest);
    void deleteProduct(Long id);
}
