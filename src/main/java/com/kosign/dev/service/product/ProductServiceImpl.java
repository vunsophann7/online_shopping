package com.kosign.dev.service.product;

import com.kosign.dev.domain.category.Category;
import com.kosign.dev.domain.category.CategoryRepository;
import com.kosign.dev.domain.product.Product;
import com.kosign.dev.domain.product.ProductRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.product.ProductMainResponse;
import com.kosign.dev.payload.product.ProductRequest;
import com.kosign.dev.payload.product.ProductResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    @Override
    public void createProduct(ProductRequest productRequest) {
        var productEntity = Product.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .stock(productRequest.getStock())
                .status("1")
                .build();
        Category categoryFound = categoryRepository.findById(productRequest.getCategory_id()).orElseThrow(() -> new CusNotFoundException("Category not found!!!"));
        productEntity.setCategory(categoryFound);
        productRepository.save(productEntity);
    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new CusNotFoundException("Product not found!!!"));
    }

    @Override
    public ProductMainResponse getProductList() {
        var productList = productRepository.findAll();
        List<ProductResponse> productResponses = productList.stream()
                .map(product -> ProductResponse.builder()
                        .name(product.getName())
                        .description(product.getDescription())
                        .price(product.getPrice())
                        .stock(product.getStock())
                        .category(product.getCategory())
                        .build()
                ).collect(Collectors.toList());
        return new ProductMainResponse(productResponses);
    }

    @Override
    public Object updateProduct(Long id, ProductRequest productRequest) {
        var productFound = getProductById(id);
        productFound.setName(productRequest.getName());
        productFound.setDescription(productRequest.getDescription());
        productFound.setPrice(productRequest.getPrice());
        productFound.setStock(productRequest.getStock());
        if (productRequest.getCategory_id() != null) {
            Category category = categoryRepository.findById(productRequest.getCategory_id())
                    .orElseThrow(() -> new CusNotFoundException("Category not found!!!"));
            productFound.setCategory(category);
        }
        return productRepository.save(productFound);
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteProductById(id);
    }
}
