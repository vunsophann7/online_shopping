package com.kosign.dev.service.shipment;

import com.kosign.dev.domain.shipment.Shipment;
import com.kosign.dev.payload.shipment.ShipmentRequest;

public interface ShipmentService {
    void createShipment(ShipmentRequest shipmentRequest);
    Object getShipmentById(Long id);
    Object getShipmentList();
    void updateShipment(Long id, ShipmentRequest shipmentRequest);
}
