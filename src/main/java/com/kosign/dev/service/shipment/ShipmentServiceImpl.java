package com.kosign.dev.service.shipment;

import com.kosign.dev.domain.shipment.Shipment;
import com.kosign.dev.domain.shipment.ShipmentRepository;
import com.kosign.dev.exception.CusNotFoundException;
import com.kosign.dev.payload.shipment.ShipmentRequest;
import com.kosign.dev.payload.shipment.ShipmentResponse;
import com.kosign.dev.service.customer.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShipmentServiceImpl implements ShipmentService {
    private final ShipmentRepository shipmentRepository;
    private final CustomerService customerService;

    @Override
    public void createShipment(ShipmentRequest shipmentRequest) {
        var shipmentEntity = Shipment.builder()
                .shipment_date(LocalDateTime.now())
                .address(shipmentRequest.getAddress())
                .city(shipmentRequest.getCity())
                .state(shipmentRequest.getState())
                .country(shipmentRequest.getCountry())
                .zip_code(shipmentRequest.getZip_code())
                .customer(customerService.getCustomerById(shipmentRequest.getCustomer_id()))
                .build();
        shipmentRepository.save(shipmentEntity);
    }

    @Override
    public Object getShipmentById(Long id) {
        ShipmentResponse shipmentResponse = shipmentRepository.findById(id)
                .map(shipment -> ShipmentResponse.builder()
                        .id(shipment.getId())
                        .shipment_date(shipment.getShipment_date())
                        .address(shipment.getAddress())
                        .state(shipment.getState())
                        .country(shipment.getCountry())
                        .zip_code(shipment.getZip_code())
                        .customer(customerService.getCustomerById(shipment.getId()))
                        .build())
                .orElseThrow(() -> new CusNotFoundException("Shipment not found!!!"));
        return shipmentResponse;
    }

    @Override
    public Object getShipmentList() {
        var shipmentList = shipmentRepository.findAll();
        List<ShipmentResponse> shipmentResponses = shipmentList.stream()
                .map(shipment -> ShipmentResponse.builder()
                        .id(shipment.getId())
                        .shipment_date(shipment.getShipment_date())
                        .address(shipment.getAddress())
                        .city(shipment.getCity())
                        .state(shipment.getState())
                        .country(shipment.getCountry())
                        .zip_code(shipment.getZip_code())
                        .customer(shipment.getCustomer())
                        .build()
                ).collect(Collectors.toList());
        return shipmentResponses;
    }

    @Override
    public void updateShipment(Long id, ShipmentRequest shipmentRequest) {
        var shipmentId = shipmentRepository.findById(id).orElseThrow(() -> new CusNotFoundException("Shipment not found!!!"));
        shipmentId.setShipment_date(shipmentRequest.getShipment_date());
        shipmentId.setAddress(shipmentRequest.getAddress());
        shipmentId.setCity(shipmentRequest.getCity());            
        shipmentId.setState(shipmentRequest.getState());
        shipmentId.setCountry(shipmentRequest.getCountry());
        shipmentId.setZip_code(shipmentRequest.getZip_code());
        shipmentId.setCustomer(customerService.getCustomerById(shipmentRequest.getCustomer_id()));
        shipmentRepository.save(shipmentId);
    }
}
